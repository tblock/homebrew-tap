# Homebrew Tap

This is a third-party repository (Tap) for the Homebrew package manager.

## Enable the repository

```shell
brew tap tblock/tap https://codeberg.org/tblock/homebrew-tap
```

## Installing TBlock

```shell
brew install tblock
echo alias sudo='sudo env PATH="$PATH:/home/linuxbrew/.linuxbrew/bin"' >> .bashrc
```

## Removing TBlock

```shell
brew uninstall tblock
```

## Remove the repository

```shell
brew untap tblock/tap
```

